var agregarPri = document.getElementById("agregarPri")
var agregarULt = document.getElementById("agregarUlt")
var eliminarPri = document.getElementById("eliminarPri")
var eliminarUlt = document.getElementById("eliminarUlt")

var arrayAutos = []

var actualizarLista = document.getElementById("actualizarLista")

agregarPri.addEventListener("click", function () {
    var auto = document.getElementById("auto").value
    arrayAutos.unshift(auto)
    console.log(arrayAutos)
})

agregarUlt.addEventListener("click", function () {
    var auto = document.getElementById("auto").value
    arrayAutos.push(auto)
    console.log(arrayAutos)
})

eliminarPri.addEventListener("click", function () {
    var auto = document.getElementById("auto").value
    arrayAutos.pop(auto)
    console.log(arrayAutos)
})

eliminarUlt.addEventListener("click", function () {
    var auto = document.getElementById("auto").value
    arrayAutos.shift(auto)
    console.log(arrayAutos)
})

var listaAutos = document.getElementById("listaAutos")
actualizarLista.addEventListener("click", function () {
    listaAutos.innerHTML = ""
    for (var i = 0; i < arrayAutos.length; i++) {
        listaAutos.innerHTML += "<li>" + arrayAutos[i] + "</li>"
    }
})
