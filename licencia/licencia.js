var enviado = document.getElementById("enviar");
enviado.disabled = true;

var nombre = document.getElementById("nombre");
var apellido = document.getElementById("apellido");
var edad = document.getElementById("edad");
var tieneLicencia = document.getElementById("tieneLicencia");
var fechaExpiracion = document.getElementById("fechaExpiracion");

nombre.addEventListener("input", function(){
    var todoCompleto = nombre.value !== "" && apellido.value !== "" && edad.value !== "" && (tieneLicencia.value === "no" || (tieneLicencia.value === "si" && fechaExpiracion.value !== ""));
    if (todoCompleto){
        enviado.disabled = false;
    } else {
        enviado.disabled = true;
    }
});

apellido.addEventListener("input", function(){
    var todoCompleto = nombre.value !== "" && apellido.value !== "" && edad.value !== "" && (tieneLicencia.value === "no" || (tieneLicencia.value === "si" && fechaExpiracion.value !== ""));
    if (todoCompleto){
        enviado.disabled = false;
    } else {
        enviado.disabled = true;
    }
});

edad.addEventListener("input", function(){
    var todoCompleto = nombre.value !== "" && apellido.value !== "" && edad.value !== "" && (tieneLicencia.value === "no" || (tieneLicencia.value === "si" && fechaExpiracion.value !== ""));
    if (todoCompleto){
        enviado.disabled = false;
    } else {
        enviado.disabled = true;
    }
});

fechaExpiracion.disabled = true;

tieneLicencia.addEventListener("input", function(){
    if (tieneLicencia.value  === "si"){
        fechaExpiracion.disabled = false;

        fechaExpiracion.addEventListener("input", function(){
            var todoCompleto = nombre.value !== "" && apellido.value !== "" && edad.value !== "" && (tieneLicencia.value === "no" || (tieneLicencia.value === "si" && fechaExpiracion.value !== ""));
            
            if (todoCompleto){
                enviado.disabled = false;
            } else {
                enviado.disabled = true;
            }
        });
    } else {
        fechaExpiracion.disabled = true;
    
        var todoCompleto = nombre.value !== "" && apellido.value !== "" && edad.value !== "" && (tieneLicencia.value === "no" || (tieneLicencia.value === "si" && fechaExpiracion.value !== ""));
        
        if (todoCompleto){
            enviado.disabled = false;
        } else {
            enviado.disabled = true;
    }
    }
});

enviado.addEventListener("click", function(){
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var edad = document.getElementById("edad").value;
    parseInt(edad);
    var tieneLicencia = document.getElementById("tieneLicencia").value;
    var fechaExpiracion = document.getElementById("fechaExpiracion").value;
    parseInt(fechaExpiracion);
    
    var esMayor = edad >= 18;
    var tieneLicencia = tieneLicencia === "si";
    var fechaActual = 20210509;
    var fechaValida = fechaExpiracion > fechaActual;

    puedeConducir = esMayor && tieneLicencia;

    if (puedeConducir) {
        if (fechaValida) {
            mensaje = nombre + " " +  apellido + " " + "está habilitado para conducir";
        } else {
            mensaje = nombre + " " +  apellido + " " + "está habilitado para conducir pero no puede hacerlo por tener una licencia caducada";
        }
    } else {
        mensaje = nombre + " " +  apellido + " " + "no está habilitado para conducir";
    }

    console.log(mensaje);    
});