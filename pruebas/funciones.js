/*
var boton = document.getElementById("boton")
boton.addEventListener("click", function () {
    var hobbies = document.getElementsByName("hobbies[]")
    console.log(hobbies)
})

const holaMudoConst = function () {
    console.log("Hola mundo!")
}

function holaMundo(n = 0) {
    do {
        console.log("holaMundo")
        n++
    } while (n <= 5)
}

function prueba(){
    const sale = "saliendo rey"
    let nombre = "David"
    let mensaje = sale + " " + nombre
    console.log(mensaje)
}

let scopefuera = "globalizado"

let funcioncita = function(){
    console.log(scopefuera)
}

const esParArrow = (num) => num % 2 === 0;

function esParDeclarada(num) {
    return num % 2 === 0
}

const esParDeclaradaVariable = function (num) {
    return num % 2 === 0
}

const cuadrado = (n) => n**2

const cuadrado2 = function (n) {
    return n**2
}

function cuadrado3(n) {
    return n**2
}

const factorial = function(num) {
    if (num === 0 || num === 1){
        return 1
    } else {
        return num * factorial(num - 1)
    }
}

const factorial2 = function(num) {
    let resultado = 1
    while (num >= 1){
        resultado *= num
        num -= 1 
    }
    return resultado
}
*/

let animales = ["perro", "gato", "loro"]

animales.forEach((animal) => {
    console.log(`Animal: ${animal}`)
})