// dada cadena recorrerla e invertirla sin funciones nativas del lenguaje
let listaTest = ["hola", "chau", "que onda perro", "o gatito", "gataso"]

function respuesta (lista) {
    for (let i = 0; i < Math.floor(lista.length / 2); i++) {
        let a = lista[i]
        let b = lista[(lista.length) - (i+1)]

        lista[(lista.length) - (i+1)] = a
        lista[i] = b
    }
    return lista
}