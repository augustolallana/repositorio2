/*
class Estudiante {
    constructor (nombre, apellido, edad, nacionalidad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nacionalidad = nacionalidad;
        this.edad = edad;
        this.ganasDeAprender = true;
    }

    mostrarDatos () {
        let datos = `Nombre: ${this.nombre}\n` +
                    `Apellido: ${this.apellido}\n` +
                    `Nacionalidad: ${this.nacionalidad}\n` + 
                    `Edad: ${this.edad}\n` + 
                    `Ganas de aprender: ${this.ganasDeAprender}`
        
        console.log(datos);
    }

    tieneGanas () {
        if (this.ganasDeAprender) {
            console.log(`${this.nombre} si tiene ganas de aprender`);
        } else {
            console.log(`${this.nombre} no debería estar en Acámica`);
        }
    }
}

const mostrarEstudiantes = document.getElementById("mostrarEstudiantes")
const enviado = document.getElementById("enviarForm");
enviado.disabled = true;

var nombre = document.getElementById("nombre");
var apellido = document.getElementById("apellido");
var edad = document.getElementById("edad");
var nacionalidad = document.getElementById("nacionalidad");

const chequearFormLleno = function () {
    let formLleno = nombre.value !== "" && apellido.value !== "" && edad.value !== "" && nacionalidad.value !== "";
    return formLleno;
}

nombre.addEventListener("input", function (){
    if (chequearFormLleno()) {
        enviado.disabled = false;
    } else {
        enviado.disabled = true
    }
})

apellido.addEventListener("input", function (){
    if (chequearFormLleno()) {
        enviado.disabled = false;
    } else {
        enviado.disabled = true
    }
})

edad.addEventListener("input", function (){
    if (chequearFormLleno()) {
        enviado.disabled = false;
    } else {
        enviado.disabled = true
    }
})

nacionalidad.addEventListener("input", function (){
    if (chequearFormLleno()) {
        enviado.disabled = false;
    } else {
        enviado.disabled = true
    }
})


let listaEstudiantes = [];

enviado.addEventListener("click", function(){
    enviado.disabled = true

    let nombreForm = nombre.value;
    let apellidoForm = apellido.value;
    let edadForm = edad.value;
    let nacionalidadForm = nacionalidad.value;

    nombre.value = "";
    apellido.value = "";
    edad.value = ""
    nacionalidad.value = ""

    let estudiante = new Estudiante(nombreForm, apellidoForm, edadForm, nacionalidadForm);
    listaEstudiantes.push(estudiante);
})

mostrarEstudiantes.addEventListener("click", function(){
    for (let i = 0; i < listaEstudiantes.length; i++) {
        listaEstudiantes[i].mostrarDatos()
    }
})
*/

class Prueba {
    constructor(pirmero, segundo){
        this.pirmero = pirmero
        this.segundo = segundo
        this.estado = [true, false, false]
    }
}

let perro = new Prueba(1,2)
console.log(perro)
console.log(perro.estado)
console.log(perro.estado[0])
console.log(perro.estado[1])
console.log(perro.estado[2])