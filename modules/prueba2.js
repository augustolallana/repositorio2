import { autoBrumBMX } from "./prueba1.js"; 

const cambiarMotor = (auto) => {
    if (auto.motor !== "BMX - 12") {
        autoBrumBMX(auto)
        return `El nuevo motor del auto ${auto.id} es ${auto.motor}`
    }
}

export { cambiarMotor }
