class Perro {
    constructor(id, raza, edad) {
        this.id = id
        this.raza = raza
        this.edad = edad
        
        // ["para adoptar", "en adopcion", "adoptado"]
        this.estado = [true, false, false]
    }
    
    darEstadoPerro() {
        let estadoStr = ["para adoptar", "en adopción", "adoptado"]
        for (let i = 0; i < this.estado.length; i++) {
            if(this.estado[i]) {
                return estadoStr[i]
            }
        }
    }

    modificarEstadoPerro(nuevoEstado) {
        let estadoStr = ["para adoptar", "en adopcion", "adoptado"]
        let estadoBool = []
        let estadoActual = this.darEstadoPerro()
        
        if (estadoActual === nuevoEstado) {
            return `El estado actual del perro ya es ${estadoActual}`
        } else {
            for (let i = 0; i < this.estado.length; i++) {
                estadoBool.push(nuevoEstado === estadoStr[i])
            }
        }
        this.estado = estadoBool
    }
}

const darDatosPerros = () => {
    let datos = ""
    for (let i = 0; i < arrayPerros.length; i++) {
        datos += `Id: ${arrayPerros[i].id}\nRaza: ${arrayPerros[i].raza}\nEdad: ${arrayPerros[i].edad} meses\nEstado: ${arrayPerros[i].darEstadoPerro()}\n\n`
    }
    if (datos === "") {
        return "No hay perros registrados"
    }
    return datos
}

const darDatosPerrosPorEstado = (estado) => {
    let datos = ""
    for (let i = 0; i < arrayPerros.length; i++) {
        if (arrayPerros[i].darEstadoPerro() === estado) {
            datos += `Id: ${arrayPerros[i].id}\nRaza: ${arrayPerros[i].raza}\nEdad: ${arrayPerros[i].edad} meses\nEstado: ${arrayPerros[i].darEstadoPerro()}\n\n`
        }
    }
    if (datos === "") {
        return `No hay perros registrados con el estado ${estado}`
    }
    return datos
}


perro1 = new Perro(1, "bulldog", 3)
perro2 = new Perro(2, "salchicha", 7)
perro3 = new Perro(3, "pitbull", 1)
perro4 = new Perro(4, "caniche", 9)
perro5 = new Perro(5, "golden", 0)
perro6 = new Perro(6, "pitbull", 1)

let arrayPerros = [perro1, perro2, perro3, perro4, perro5, perro6]

