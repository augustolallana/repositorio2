const calculator = require("./modules/calculator.js");
const fs = require("fs");

let suma = calculator.sumar(5, 6);
let resta = calculator.restar(11, 8);
let multiplicacion = calculator.multiplicar(3, 9);
let division1 = calculator.dividir(12, 6);
let division2 = calculator.dividir(5, 0);

let operaciones = `Suma 5 + 6: ${suma}, Resta 11 - 8: ${resta}, Multiplicación 3 x 9: ${multiplicacion}, División 12 / 6: ${division1}, División 5 / 0: ${division2}`

fs.writeFile("operacionesMatematicas.txt", operaciones, (err) => {
    if (err) {
        console.log(err)
    }
    console.log("Archivo creado con éxito")
    
    fs.readFile("./operacionesMatematicas.txt", "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        }
        console.log(data)
    })
})