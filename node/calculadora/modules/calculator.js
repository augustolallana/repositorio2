const sumar = (n1, n2) => {
    return n1 + n2
}

const restar = (n1, n2) => {
    return n1 - n2
}

const multiplicar = (n1, n2) => {
    return n1 * n2
}

const dividir = (n1, n2) => {
    if (n2 !== 0) {
        return n1 / n2
    } else {
        return new Error("No se puede dividir por 0!")
    }
}

exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;