const fs = require("fs");

let arrayHobbies = ["Programar", "Jugar al fútbol", "Juntarme con amigos"]

/*
//Primera opción

arrayHobbies.forEach(hobby => {
    console.log(`Hobby: ${hobby}`)

    fs.writeFile(`${hobby}.txt`, `Este es el archivo del hobby ${hobby}`, (err, data) => {
        if (err) {
            console.log(err)
        }

        console.log(`Archivo del hobby ${hobby} creado exitosamente`)
    })
})
*/

//Segunda opción

fs.writeFile("hobbies.txt", arrayHobbies.join("\n"), (err) => {
    console.log("Archivo creado con éxito")
})

